/* eslint-disable */

export const axios_defaults_baseURL =
  process.env.NODE_ENV === "production"
    ? "https://murrengan.ru"
    : "http://localhost:8000";
export const axios_defaults_baseFrontURL =
  process.env.NODE_ENV === "production"
    ? "https://murrengan.ru"
    : "http://localhost:8080";

export const websocket_base_url =
  process.env.NODE_ENV === "production"
    ? "wss://murrengan.ru"
    : "ws://127.0.0.1:8000";

export const tavernSocketUrl = "/ws/murr_chat/murr_tavern/";
export const battleLobbyUrl = "/ws/murr_battle_room/lobby/";
export const murrWebSocketTypes = {
  MURR_CHAT: "murr_chat",
  MURR_BATTLE_ROOM: "murr_battle_room",
};

export const siteKey = "6Lc0qP4UAAAAADXDdZYU_wnfeDJfTRKRPVU9h046";
export const murrOauthGoogle =
  "688686297345-1g9g2n42dko32vlt11gofep9dfu4bcq1.apps.googleusercontent.com";
export const DISABLE_RECAPTCHA = process.env.NODE_ENV !== "production";

export const newMurrInReleaseStatus = "release";

export const VK_CLIENT_ID = "7652183";
export const VK_REDIRECT_URI = `${axios_defaults_baseFrontURL}/oauth/vk`;
