from django.core.management import BaseCommand

from murren.models import TrustedRegistrationEmail


WHITE_LIST_EMAILS = [
  "inbox.ru",
  "list.ru",
  "bk.ru",
  "i.ua",
  "ua.fm",
  "email.ua",
  "3g.ua",
  "football.ua",
  "gmail.com",
  "yahoo.com",
  "hotmail.com",
  "aol.com",
  "hotmail.co.uk",
  "hotmail.fr",
  "msn.com",
  "yahoo.fr",
  "wanadoo.fr",
  "orange.fr",
  "comcast.net",
  "yahoo.co.uk",
  "yahoo.com.br",
  "yahoo.co.in",
  "live.com",
  "rediffmail.com",
  "free.fr",
  "gmx.de",
  "web.de",
  "yandex.ru",
  "yandex.kz",
  "yandex.ua",
  "ymail.com",
  "ukr.net",
  "libero.it",
  "outlook.com",
  "uol.com.br",
  "bol.com.br",
  "mail.ru",
  "cox.net",
  "hotmail.it",
  "sbcglobal.net",
  "sfr.fr	",
  "live.fr",
  "verizon.net",
  "live.co.uk	",
  "googlemail.com",
  "yahoo.es",
  "ig.com.br",
  "live.nl",
  "bigpond.com",
  "terra.com.br",
  "yahoo.it",
  "neuf.fr",
  "yahoo.de",
  "alice.it",
  "rocketmail.com",
  "att.net",
  "laposte.net",
  "facebook.com",
  "bellsouth.net",
  "yahoo.in",
  "hotmail.es",
  "charter.net",
  "yahoo.ca",
  "yahoo.com.au",
  "rambler.ru",
  "hotmail.de",
  "tiscali.it",
  "shaw.ca",
  "yahoo.co.jp",
  "sky.com",
  "earthlink.net",
  "optonline.net",
  "freenet.de",
  "t-online.de",
  "aliceadsl.fr",
  "virgilio.it",
  "home.nl",
  "qq.com",
  "telenet.be",
  "me.com",
  "yahoo.com.ar",
  "tiscali.co.uk",
  "yahoo.com.mx",
  "voila.fr",
  "gmx.net",
  "mail.com",
  "planet.nl",
  "tin.it",
  "live.it",
  "ntlworld.com",
  "arcor.de	",
  "yahoo.co.id",
  "frontiernet.net",
  "hetnet.nl",
  "live.com.au",
  "yahoo.com.sg",
  "zonnet.nl",
  "club-internet.fr",
  "juno.com",
  "optusnet.com.au",
  "blueyonder.co.uk",
  "bluewin.ch",
  "skynet.be",
  "sympatico.ca",
  "windstream.net",
  "mac.com",
  "centurytel.net",
  "chello.nl",
  "live.ca",
  "aim.com",
  "bigpond.net.au",
]


class Command(BaseCommand):
    help = 'Сгенерировать начальный список почтовых сервисов доступных для  регистрации'

    def handle(self, *args, **options):
        clear = options.get('clear')

        if clear:
            TrustedRegistrationEmail.objects.all().delete()

        if not TrustedRegistrationEmail.objects.count():
            TrustedRegistrationEmail.objects.bulk_create(
                [TrustedRegistrationEmail(service_domain=service) for service in WHITE_LIST_EMAILS]
            )
            self.stdout.write(self.style.SUCCESS('Список белых почтовых сервисов успешно создан'))
        else:
            self.stdout.write(self.style.WARNING(
              'В базе уже существуют почтовые сервисы, чтобы создать начальный список, необходимо очистить все записи. '
              'Для этого воспользуйтесь флагом --clear при запуске команды. '
              'python manage.py create_white_emails --clear'
          ))

    def add_arguments(self, parser):
        parser.add_argument(
          '-c', '--clear', action='store_const', const=True,
          help="Удаляет все записи из таблицы и создает начальный список сервисов"
        )
